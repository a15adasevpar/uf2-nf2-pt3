![NovaTascaCreada.png](NovaTascaCreada.png)

### Configuraci� de Symfony ###

[Manual de Digital Ocean](https://www.digitalocean.com/community/tutorials/how-to-deploy-a-symfony-application-to-production-on-ubuntu-14-04)

### Modificacions respecte al manual ###

* En comptes de fer servir l'ordre **sudo apt-get install git php5-cli php5-curl acl** fem servir **sudo apt-get install git php-cli php-curl**, ja que la versi� que fem servir de php �s la 7.0 i no la 5.0. L'ordre **apt-get install** ja detecta quina �s la �ltima versi� per descarregar.

* El fitxer de configuraci� my.cnf ha canviat de nom i d'ubicaci�. Per editar el fitxer de configuraci� farem servir l'ordre **sudo nano /etc/mysql/mysql.conf.d/mysqld.cnf**

* Quan accedim al fitxer de configuraci� **/etc/php5/apache2/php.ini** en comptes de posar la zona hor�ria d'Ansterdam (**date.timezone = Europe/Amsterdam**) posem **date.timezone = Europe/Madrid**

* La directiva que hem d'aplicar a **/etc/apache2/sites-available/000-default.conf** ha canviat. La l�nia **Order Allow,Deny** l'hem de substituir per **Require all granted**. La l�nia **Allow from All** s'elimina, no s'ha de posar.
